//
//  FindONEViewController.swift
//  CoreDataForDummies
//
//  Created by alejo on 30/1/18.
//  Copyright © 2018 alejo. All rights reserved.
//

import UIKit
import CoreData

class FindONEViewController: UIViewController {
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    var person:Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = person?.name!
        addressLabel.text = person?.address!
        phoneLabel.text = person?.phone!

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func deleteBut(_ sender: Any) {
            let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        manageObjectContext.delete(person!)
        do{
            try manageObjectContext.save()
            
        }catch{
            print("Error")
        }
    }
    

}
