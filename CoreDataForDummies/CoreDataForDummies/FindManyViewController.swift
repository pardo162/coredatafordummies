//
//  FindManyViewController.swift
//  CoreDataForDummies
//
//  Created by alejo on 30/1/18.
//  Copyright © 2018 alejo. All rights reserved.
//

import UIKit
import CoreData


class FindManyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var tabla: UITableView!
     var persons:[Person] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLable.text = persons [0].name
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
            for section in 0..<tableView.numberOfSections {
                for row in 0..<tableView.numberOfRows(inSection: section) {
                    let indexPath = NSIndexPath(row: row, section: section)
                    var cell = tableView.cellForRow(at: indexPath as IndexPath)
                    cell?.textLabel?.text = "Nombre: \(self.persons[row].name!) Team: \(self.persons[row].address ?? "No Team")"
                    // do what you want with the cell
                    
                }
            }
        return cell
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
