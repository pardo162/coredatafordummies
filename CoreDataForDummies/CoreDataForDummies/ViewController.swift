//
//  ViewController.swift
//  CoreDataForDummies
//
//  Created by alejo on 23/1/18.
//  Copyright © 2018 alejo. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    var siglePerson:Person?
    var persons:[Person] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func find(_ sender: Any) {
        if nameTextField.text == ""
        {findaAll()}
        else
        {
            findaOne()
        }
        
        
    }
    func findaAll(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            var results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>) )
           // for result in results{
            persons = results as! [Person]
            performSegue(withIdentifier: "FindMany", sender: self)
            
        }catch{
            print("Error")
        }
        
    }
    func findaOne(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        request.entity = entityDescription
        let predicate = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = predicate
        do {
            var results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>) )
            if results.count > 0 {
                let match = results[0] as! Person
                siglePerson = match
                performSegue(withIdentifier: "FindONE", sender: self)
              
                
            }
            else
            {
                persons = results as! [Person]
                performSegue(withIdentifier: "FindMany", sender: self)
                
            }
        }catch{
            print("Error")
        }
        
    }
    @IBAction func save(_ sender: Any) {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let person = Person(entity: entityDescription!, insertInto: manageObjectContext)
        person.address = addressTextField.text ?? ""
        person.name = nameTextField.text ?? ""
        person.phone = phoneTextField.text ?? ""
        
        do{
            try manageObjectContext.save()
            addressTextField.text = ""
            nameTextField.text = ""
            phoneTextField.text = ""
        } catch{
            print("Error")
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FindONE"{
            let destination = segue.destination as! FindONEViewController
            destination.person = siglePerson
        }
        if segue.identifier == "FindMany"{
            let destination = segue.destination as! FindManyViewController
            destination.persons = persons
        }
        
    }
    
}

